---
title: The Documentation of the Schedule Website
geekdocNav: false
geekdocAlign: center
geekdocAnchor: false
---

Hello! On this website you can find a documentation for the InnTime project.

You can find docs for Development process

{{< button size="large" relref="docs/dev/" >}}Development{{< /button >}}

You can find docs for work with API

{{< button size="large" relref="docs/api/" >}}API Guide{{< /button >}}

You can find docs how to use website as a user

{{< button size="large" relref="docs/user/" >}}User Guide{{< /button >}}


Contacts

n.grigorenko@innopolis.university
i.yusupov@innopolis.university
i.chebykin@innopolis.university