---
title: "Developer Documentation"
weight: 1
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

# Introduction

<!--more-->

{{< toc >}}

# Documentation purpose

**Onboarding New Developers**: It provides a roadmap for new developers to get familiar with the project's architecture, codebase, and development environment. This helps reduce the time and effort required for new team members to become productive

**Understanding Functionality**: Developer documentation explains the functionality of various components, modules, classes, and methods within the software. This clarity enables developers to understand how different parts of the system work together to achieve specific goals

**Best Practices and Guidelines**: It outlines coding standards, design patterns, and best practices to be followed when contributing to the codebase. This ensures consistency across the project and helps maintain code quality.

# Documentation audience

Development team, onbording developers, project manager, and stakeholders

# Tech stack

- Frontend: TS, css modules, React, Mobx
- Backend: Python, Django

# Structure of the project

<img src="/project-architecture.png">

# Frontend

https://github.com/InnTime/InnTime-frontend

## Frontend stack

- TS - statically-typed superset of JavaScript that enhances developer in-time testing
- React - framework to build modern SPA
- Mobx - State manager to share functionality between different visual components
- axios - HTTP Client to work with backend
- @fullcalendar - library to display responsive calendar
- ics - to save calendar in .ics format to use in supported calendars like Google Calendar, Microsoft Outlook, Apple Calendar

## Structure of frontend. Visual part

### App.tsx

main file that starts the app

```ts
...
const App = () => {
    const {auth} = useContext(Context);

    useEffect(() => { // here implementend automated reauth
        if (localStorage.getItem('token')) {
            auth.setAuth(true);
        }
    }, [])

    return (
        <BrowserRouter>
            <AppRouter/> // main component, connects to routes.tsx
        </BrowserRouter>
    );
};
...
```

### routes.tsx

contains connection between URL and pages

```ts
export const authRoutes = [
  {
    path: HOME_ROUTE, // browser url
    element: <Homepage />, // react visual component
  },
];

export const publicRoutes = [
  {
    path: LOGIN_ROUTE,
    element: <LoginForm />,
  },
];
```

### pages/

represent page content

HomePage

```ts
const HomePage = () => {
  return (
    <div>
      <HomeSection /> // component
      <AddSection /> // another component
      <CalendarSection /> // third component
    </div>
  );
};
```

### components/

independant blocks, which used in pages

AddSection.tsx

```ts

const AddSection = () => {
    const {group, elective, event} = useContext(Context); // mobx shared stores

    const [cardsTypeFilter, setCardsTypeFilter] = useState({
        scheduleCardsType: {value: 'group', name: 'Group'},
    }) // just dynamic state that can change

    const [fetchGroups, isGroupsLoading, groupsError] = useFetching(async () => {
        const response = await GroupService.fetchGroups();
        group.setCards(response.data);

        const userGroupResponse = await GroupService.fetchUserGroupId();
        const selectedGroupId = userGroupResponse.data;
        if (selectedGroupId) {
            group.setSelectedCards(group.cards.filter((card) => card.id === selectedGroupId))
            await event.updateCourses();
        } else {
            group.setFilteredCards(group.cards)
        }
    }) // handy custom common hook, used to fetch and prepare data from backend to frontend

  useEffect(() => {
        fetchGroups();
        fetchElectives();
    }, []) // make calls when page renders

    return (
        <div className={cl.addSection} id='add-section'>
            <div className="container">
                <section className={cl.addSection__header}>
                    <h1 className={cl.addSection__title}>
                        InnTime
                    </h1>
                 ...
                </section>
            </div>
        </div>
    ); // return component
```

### hooks/

common function handlers that use in many components

```ts
export const useFetching = (
  callback: () => any
): [() => Promise<void>, boolean, string] => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");

  const fetching = async () => {
    try {
      setIsLoading(true); // start fetch date, set load state to
      await callback(); // return positive result
    } catch (e) {
      // @ts-ignore
      setError(e.message); // catch error
    } finally {
      setIsLoading(false); // when call finished or error happen, reset loading state
    }
  };
  return [fetching, isLoading, error];
};
```

## Structure of frontend. Backend data manipulation part

### http/index.ts

entry point with settings for HTTP request/response between browser and server

```ts
import axios from "axios";

const $api = axios.create({
  withCredentials: true,
  baseURL: process.env.REACT_APP_API_URL,
}); // basic setup with defined backend api route

$api.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
}); // auth headers for requests with JWT-token checking

$api.interceptors.response.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
});

export default $api;
```

### services/

repsent endpoints to API

```ts
export default class AuthService {
  static async login(
    email: string,
    password: string
  ): Promise<AxiosResponse<AuthResponse>> {
    return $api.post<AuthResponse>("/login", { email, password }); // call /login with POST http method with {email, password} payload body
  }

  static async registration(
    email: string,
    password: string
  ): Promise<AxiosResponse<AuthResponse>> {
    return $api.post<AuthResponse>("/register", { email, password });
  }

  static async logout(): Promise<AxiosResponse<AuthResponse>> {
    return $api.post<AuthResponse>("/logout");
  }
}
```

### models/

contains types for objects that goes from backend to frontend

ICourse.ts

```ts
//  types for keys of object
export interface ICourse {
  id: number;
  name: string;
  room: number;
  teacher: string;
  start_time: string;
  end_time: string;
  day_of_week:
    | "Monday"
    | "Tuesday"
    | "Wednesday"
    | "Thursday"
    | "Friday"
    | "Saturday"
    | "Sunday";
}
```

## run frontend

```
npm i
npm run start
```

# Backend

https://github.com/InnTime/inn-time-backend

## Backend stack

- Python - commonly known rapidly development language for its syntax and huge community
- Flask - web lightweight framework for creating fast backend and API
- PyJWT - library for creating and using JWT-tokens in authentication
- docker - enables seamless containerization to build, ship, and run applications efficiently across any environment

## Structure of backend

### manage.py

main fiie that runs script

```python
from app import create_app

if __name__ == '__main__':
    app = create_app()
    app.run()
```

### app/admin.py

```python
from flask_admin.contrib.sqla import ModelView


class AdminView(ModelView):
    column_display_pk = True
```

The AdminView class extends ModelView, a class provided by Flask-Admin for generating views based on SQLAlchemy models.

By setting `column_display_pk` to True, it ensures that the primary key (pk) of each record in the database table is displayed in the list view of the Flask-Admin interface, allowing administrators to easily identify and manage records based on their primary keys.

### app/blueprints/-/models.py

discribes db tables

```python
class Group(db.Model):
    __tablename__ = 'groups'

    id = db.Column(db.Integer, primary_key=True) # id integer column
    group_name = db.Column(db.String(15), unique=True, nullable=False) # group_name string column
    type = db.Column(Enum('B', 'M', name='group_type_enum')) # column where only 'B' and 'M' are allowed values
    year = db.Column(db.Integer)
    number = db.Column(db.Integer)

    users = db.relationship('User', back_populates='group') # one-to-many relationship between Group and User
    courses = db.relationship('CoursesDistribution', back_populates='group')

    def __str__(self):
        return self.group_name
```

### app/blueprints/-/[^models.py]

describes mutated basic tables described in models.py

```python
from flask_admin.contrib.sqla import ModelView


class GroupView(ModelView):
    column_display_pk = True
    form_excluded_columns = ['users', 'courses']


class CourseDistributionView(ModelView):
    column_display_pk = True
    column_list = ('course', 'group')
    column_labels = {'course': 'Course', 'group': 'Group'}
```

### tests

contains test with power of pytest

```python

@pytest.fixture()
def app():
    app = create_app()
    app.config.update({
        "TESTING": True,
    })

    # other setup can go here

    yield app

    with app.app_context():
        # clean up / reset resources here
        db.session.query(User).delete()
        db.session.query(Course).delete()
        db.session.query(Group).delete()
        db.session.query(Elective).delete()
        db.session.query(CoursesDistribution).delete()
        db.session.query(ElectivesDistribution).delete()
        db.session.commit()


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()
```

Dockerfile to build a Docker image and run with Docker
Docker-compose to run docker containers

### run backend

with flask

```
pip install --upgrade pip
pip install -r requirements.txt
flask run
```

with docker-compose

```
docker-compose up
```

# FAQ

- **Where can I find the codebases for frontend and backend?**

  Frontend: https://github.com/InnTime/InnTime-frontend.git

  Backend: https://github.com/InnTime/inn-time-backend

- **What technologies are used in the frontend and backend?**

  The frontend is built using TypeScript, React, Mobx, axios, and @fullcalendar. The backend is built using Python, Flask, PyJWT, and Docker

  The details of stacks are stated in `Frontend stack` and `Backend stack` sections

- **How do I contribute to the frontend codebase?**

  To contribute to the frontend codebase, follow these steps:

  Clone the Repository: Clone the frontend repository from GitHub using the following command

  ```
  git clone https://github.com/InnTime/InnTime-frontend.git
  ```

  Install Dependencies: Navigate to the cloned directory and install dependencies using npm:

  ```
  cd InnTime-frontend
  npm install
  ```

  Run Locally and make the desired changes or additions to the codebase.

  ```
  npm run start
  ```

  Push Changes: Once satisfied with your changes, push them to a new branch in the repository:

  ```
  git checkout -b feature/your-feature-name
  git add .
  git commit -m "Add your commit message here"
  git push origin feature/your-feature-name
  ```

- **How do I contribute to the backend codebase?**

  To contribute to the backend codebase, follow these steps:

  Clone the Repository: Clone the backend repository from GitHub using the following command

  ```
  git clone https://github.com/InnTime/inn-time-backend
  ```

  Navigate to project directory and install dependencies

  ```
  cd inn-time-backend
  pip install -r requirements.txt
  ```

  Run Locally and make the desired changes or additions to the codebase.

  ```
  flask run
  ```

  Push Changes: Once satisfied with your changes, push them to a new branch in the repository:

  ```
  git checkout -b feature/your-feature-name
  git add .
  git commit -m "Add your commit message here"
  git push origin feature/your-feature-name
  ```

- **How do I run backend tests using pytest?**

  To run backend tests using pytest, follow these steps:

  Set Up Environment: Ensure that you have Python and pytest installed in your development environment.
  Navigate to the Tests Directory: Navigate to the tests directory within the backend repository.
  Run Tests: Execute the following command to run all tests:

  ```
  pytest
  ```

  View Test Results: After running the tests, pytest will display the test results indicating which tests passed, failed, or encountered errors

- **How do I deploy the backend using Docker-compose?**

  Clone backend repo, run

  ```
  docker-compose up
  ```

  Access the Backend: Once the containers are up and running, you can access the backend API at http://localhost:5000.

- **How do I access the Flask-Admin interface for database management?**

  Make the points in previos guide but navigate to http://localhost:5000/admin
