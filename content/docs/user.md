---
title: "User Documentation"
weight: 1
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

# Introduction

InnTime is a project that will help students and professors save time searching for their schedules. Just go to the website, choose your group and the entire schedule will be in front of you in real time. Our project solves the problem of inconvenient use of the existing schedule, aiming to provide quick access to the full schedule from all courses for students. The project is aimed at automating scheduling, speeding up the schedule search time for each student.

This document walks the user through the use of our project's web interface.

<!--more-->

{{< toc >}}

# User Documentation

## Authentication

<img src="/frontpage.jpg">

When you first visit our site, you will be greeted by a page where you can log in to your account or create one if you are not already registered. You also have the option to use our application without authorization, but then when you refresh the page, you will lose your created schedule.

You can login into your account or create one by clicking the `Login` button **(1)**. This will redirect you to authentication page:

<img src="/auth.jpg">

Once you are logged in to your account you can add classes to your schedule by clicking the `Add events to your calendar` button **(2)**.

You can also display your schedule if you have previously created one by clicking the `See my schedule` button **(3)**.

## Adding schedule

<img src="/add.jpg">

On this page you can choose a ready-made timetable. To make searching more convenient, there are sortings by study group **(1)** and course **(2)**, as well as a manual search **(3)** at the top.

Each timetable has buttons to export the schedule or add it to yours.

<img src="/group_109.jpg">

## Viewing schedule

When clicking on the course, the user is provided with a preview of the course with his personal schedule. He can view the schedule for each course like this.

Here the user also has posibility to export the schedule in `.icl` format **(1)**, add schedule to his/her calendar **(2)**.

<img src="/item_modal.jpg">

After the user adds courses to his schedule, he can go to the schedule page **(3)** and see his full schedule for a certain period of time.

<img src="/group_151.jpg">

Through the left bottom button **(4)** the user can log out of his/her account.

# FAQ

- **How often is the schedule updated?**

    The schedule is updated in real-time, so any changes made by instructors will be reflected immediately on the website.

- **Can I set reminders for upcoming classes or events?**

    Currently, setting reminders is not a feature of our schedule tool. However, you can manually set reminders on your device using calendar apps or other reminder tools.

- **Is there a mobile app available for accessing the schedule?**

    Currently, we only have a web interface available. However, we are working hard to create a mobile version so that our app can also be used conveniently on smartphones.

- **Can I sync my schedule with other calendar applications, such as Outlook or Apple Calendar?**

    Currently, we do not offer direct synchronization with other calendar applications. However, you can manually import your schedule into these applications using the exported Google file.

- **How can I provide feedback or suggestions for improving the schedule tool?**

    We welcome feedback and suggestions for improving our schedule tool. Please send us your feedback through the contact form on our website, and we'll take it into consideration for future updates.
