---
title: "API Documentation"
weight: 1
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

# Introduction

Below you can find a documentation of interaction with the API. API is devided into three key groups which are presented in our application: Course, Electives and Users.

<!--more-->

{{< toc >}}

# Routes

You can access the functionality of the our API by using the routes below with corresponding methods.
Here you can find the template of headers and body information for the request. As well as the responses with the corressponding status codes

## Courses
The routes for course information and interactive actions

### **GET** : /get_user_courses
```bash
Headers = {"Authorization": "Bearer your_token"}
Data: None
```

Returning type:
```bash
{
        'id': 1,
        'name': name,
        'type': type,
        'year': year,
        'number': 1
}
```

### **GET** : /get_courses
```
Headers = None
Data: None
```

Returning type: array of
```bash
{
        'id': 1,
        'name': course_name,
        'room': classroom,
        'teacher': teacher,
        'type': type,
        'start_time': start_time,
        'end_time': end_time,
        'day_of_week': day_of_week
}
```

### **GET** : /get_groups
```
Headers = None
Data: None
```

Returning type: array of 
```bash
{
        'id': 1,
        'name': name,
        'type': type,
        'year': year,
        'number': 1
}
```
## Electives
The routes for electives information and interactive actions

### **GET** : /get_user_electives
```
Headers = {"Authorization": "Bearer your_token"}
Data: None
```

Returning type:
```bash
{
    'name': elective_name,
    'room': classroom,
    'teacher': teacher,
    'type': type,
    'start_time': start_time,
    'end_time': end_time,
}
```

### **GET** : /get_electives
```
Headers = None
Data: None
```

Returning type: array of 
```bash
{
    'name': elective_name,
    'type': type,
}
```

### **POST** : /set_user_elective
```
Headers = {"Authorization": "Bearer your_token"}
Data: None
Body: {
    'name': elective_name,
    'room': classroom,
    'teacher': teacher,
    'type': type,
    'start_time': start_time,
    'end_time': end_time,
}
```

Returning type:
```bash
'Elective set successfully' with code 200
or
'User already has this elective' with code 400
or
'Invalid elective name' with code 401
```

### **DELETE** : /delete_user_elective
```
Headers = {"Authorization": "Bearer your_token"}
Data: None
Body: {
    'name': elective_name,
}
```

Returning type:
```bash
'Elective deleted successfully' with code 200
or
'Elective not found' with code 404
```

## Users
The routes for user information and interactive actions

### **POST** : /login
```
Headers = None
Data: None
Body: {
    'email': email,
    'password': password
}
```

Returning type:
```bash
'Invalid email or password' with code 401
or
{'accessToken': access_token}
```

### **POST** : /register
```
Headers = None
Data: None
Body: {
    'email': email,
    'password': password
}
```

Returning type:
```bash
'Some information is missing' with code 400
or
'User with such an email exists' with code 400
or
'Registration successful' with code 200
```

### **PUT** : /set_user_group
```
Headers = {"Authorization": "Bearer your_token"}
Data: None
Body: {
    "group_id": 1
}
```

Returning type:
```bash
'Group ID cannot be null' with code 400
or
'Group ID is not an integer' with code 400
or
'There is no group with such an id' with code 400
or
'Group set successfully' with code 200
```

### **GET** : /get_user_group
```
Headers = {"Authorization": "Bearer your_token"}
Data: None
```

Returning type:
```bash
group_id and code 200
```

### **PUT** : /unset_user_group
```
Headers = {"Authorization": "Bearer your_token"}
Data: None
```

Returning type:
```bash
'Group unset successfully' with code 200
```

### **PUT** : /update_user
```
Headers = {"Authorization": "Bearer your_token"}
Data: None
Body: {
    'password': 'new_password',
}
```

Returning type:
```bash
'User information updated successfully' with code 200
```

### **DELETE** : /logout
```
Headers = {"Authorization": "Bearer your_token"}
Data: None
```

Returning type:
```bash
"Successfully logged out" with code 200
```

# Admin Panel
Next, you can see the format of the backend admin panel and the ability to edit the course

Here you can add new courses with information
<img src="/courses_back.png">

As well as you can edit them
<img src="/courses_edit_back.png">

# FAQ

- **How do I authenticate requests to the API?**

    To authenticate requests, include the Authorization header in your request with a valid JWT token. You can obtain this token by logging in using the `/login` endpoint.

- **What are the available endpoints for retrieving course information?**

    `/get_user_courses`: Retrieves courses for a specific user.
    `/get_courses`: Retrieves all available courses.
    `/get_groups`: Retrieves groups associated with courses.

- **How can I access elective information?**

    `/get_user_electives`: Retrieves electives for a specific user.
    `/get_electives`: Retrieves all available electives.
    `/set_user_elective`: Adds an elective for a user.
    `/delete_user_elective`: Deletes an elective for a user.

- **What actions can I perform regarding user information?**

    `/login`: Logs a user in and provides an access token.
    `/register`: Registers a new user.
    `/set_user_group`: Sets a group for a user.
    `/get_user_group`: Retrieves the group of a user.
    `/unset_user_group`: Removes the group association for a user.
    `/update_user`: Updates user information.
    `/logout`: Logs a user out.

- **How do I handle errors returned by the API?**

    Errors returned by the API include status codes and corresponding messages. Common errors include invalid credentials, missing information, and resource not found. Ensure you handle these errors appropriately in your application.

- **Are there any rate limits or usage restrictions on the API?**

    Currently, there are no rate limits or usage restrictions on the API. However, we recommend using the API responsibly and avoiding excessive or unnecessary requests to ensure optimal performance for all users.

- **Can I integrate this API with my existing application?**

    Yes, our API is designed to be easily integrated into existing applications. You can use standard HTTP requests to interact with the API endpoints and incorporate the retrieved data into your application as needed.

- **Are there any plans to expand the functionality of the API in the future?**

    We are constantly working to improve and enhance the functionality of our API based on user feedback and requirements. While we don't have specific plans to announce at this time, we welcome suggestions and feature requests from our users to help shape the future development of the API.

- **What if I get an Internal Server Error?**

    Just contact us by ...@gmail.com and provide a scenario how you get this type of status code
